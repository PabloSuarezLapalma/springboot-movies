package com.example.usermanagement.controllers;
import com.example.usermanagement.entities.Movie;
import com.example.usermanagement.repositories.MoviesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.example.usermanagement.services.MovieService;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/movies")
public class MoviesController {
    @Autowired
    private MovieService movieService;

    @GetMapping
    public List<Movie> getAllMovies() {
        return movieService.getAllMovies();
    }

    @GetMapping("/movie/{id}")
    public ResponseEntity<Movie> getUserById(@PathVariable Integer id) {
        Optional<Movie> movie = movieService.getMoviebyId(id);
        return movie.map(value -> new ResponseEntity<>(value, HttpStatus.OK))
                .orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @PostMapping("/movie")
    public Movie createMovie(@RequestBody Movie movie) {
        return movieService.saveMovie(movie);
    }

    @PutMapping("/movie/{id}")
    public ResponseEntity<Movie> updateMovie(@PathVariable Integer id, @RequestBody Movie movieDetails) {
        Optional<Movie> movie = movieService.getMoviebyId(id);
        if (movie.isPresent()) {
            Movie updatedMovie= movie.get();
            updatedMovie.setTitulo(movieDetails.getTitulo());
            updatedMovie.setDescripcion(movieDetails.getDescripcion());
            updatedMovie.setFechalanzamiento(movieDetails.getFechalanzamiento());
            updatedMovie.setImagen(movieDetails.getImagen());
            return new ResponseEntity<>(movieService.saveMovie(updatedMovie), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
    @DeleteMapping("/movie/{id}")
    public ResponseEntity<HttpStatus> deleteMovie(@PathVariable Integer id) {

        movieService.deleteMovie(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
