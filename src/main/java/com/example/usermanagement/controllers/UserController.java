package com.example.usermanagement.controllers;
import com.example.usermanagement.entities.Movie;
import com.example.usermanagement.entities.User;
import com.example.usermanagement.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.example.usermanagement.services.UserService;


import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/users")
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping
    public List<User> getAllUsers() {
        return userService.getAllUsers();
    }

    @GetMapping("/user/{id}")
    public ResponseEntity<User> getUserById(@PathVariable Integer id) {
        Optional<User> user = userService.getUserbyId(id);
        return user.map(value -> new ResponseEntity<>(value, HttpStatus.OK))
                .orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @PostMapping("/user")
    public User createUser(@RequestBody User user) {
        return userService.saveUser(user);
    }

    @PutMapping("/user/{id}")
    public  ResponseEntity<User>  updateUser(@PathVariable Integer id, @RequestBody User userDetails) {
        Optional<User> user = userService.getUserbyId(id);
        if (user.isPresent()) {
            User updatedUser = user.get();
            updatedUser.setNombre(userDetails.getNombre());
            updatedUser.setApellido(userDetails.getApellido());
            updatedUser.setPassword(userDetails.getPassword());
            updatedUser.setEmail(userDetails.getEmail());
            updatedUser.setFechaNacimiento(userDetails.getFechaNacimiento());
            updatedUser.setPais(userDetails.getPais());
            return new ResponseEntity<>(userService.saveUser(updatedUser), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }

    @DeleteMapping("/user/{id}")
    public ResponseEntity<HttpStatus> deleteUser(@PathVariable Integer id) {
        userService.deleteUser(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
