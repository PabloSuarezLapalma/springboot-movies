package com.example.usermanagement.services;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.usermanagement.entities.Movie;
import com.example.usermanagement.repositories.MoviesRepository;

import java.util.List;
import java.util.Optional;

@Service
public class MovieService {

    @Autowired
    private MoviesRepository moviesRepository;

    public List<Movie> getAllMovies() {
        return moviesRepository.findAll();
    }

    public Optional<Movie> getMoviebyId(Integer id) {
        return moviesRepository.findById(id);
    }

    public Movie saveMovie(Movie pelicula) {
        return moviesRepository.save(pelicula);
    }

    public void deleteMovie(Integer id) {
        moviesRepository.deleteById(id);
    }
}
