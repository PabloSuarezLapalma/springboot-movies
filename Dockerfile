# Usar una imagen base de Java
FROM openjdk:22

# Configurar las variables de entorno
ENV APP_HOME=/usr/app/
WORKDIR $APP_HOME

# Copiar el JAR de la aplicación al contenedor
COPY build/libs/user-management-0.0.1-SNAPSHOT.jar app.jar

# Exponer el puerto en el que corre la aplicación
EXPOSE 8080

# Comando para ejecutar la aplicación
ENTRYPOINT ["java", "-jar", "app.jar"]


